# Cherubits Infrastructure

Setup and manage infrastructure of Cherubits LLC.

## VPS nodes

Whole infrastructure is hosted in VPS servers of [Servergarden](https://servergarden.hu).

### Coruscant

Services
* Internal Policy Audit
* Directory and file sharing
* Instant messaging
* Email
* Calendar and events

### Yavin

Server contains services for SDLC:
* GitLab FOSS
* Nexus Repository Manager
* SonarQube Continuous Inspection
* Jenkins Continuous Integration

### Tatooine

* Public Webpage
* Product pages
* Webshop

## Commands

Setup site

```shell script
ansible-playbook site.yml
```

Remove files

```shell script
ansible-playbook purge.yml
```